---
title: Contributors
---

**Chirayu Desai**, Lead CalyxOS Developer, has been working on Android and open source software since 2012. He has worked on all things Android, right from the hardware level code to working on various apps. He holds a Bachelor's degree in Computer Engineering and works remotely from Ahmedabad, India.

**Donald Morris**, Graphic design

**Glenn Sorrentino**, User experience

**Elijah Waxwing**, Senior Technologist, has been working on the issue of digital justice since 2000. He is a software developer, systems architect, information security specialist, and manager. He wrote his master's thesis on the political economy of surveillance capitalism.

**Hans-Christoph Steiner**, Guardian project advisor

**Michael Bestas**

**Nicholas Merrill**

**Torsten Grote**, Senior Android Developer, specializes in Free Software and related aspects of digital freedom such as privacy and security. He started the "Free Your Android" campaign to educate people about how they can use mobile devices in freedom and have their privacy respected. Today, he works on filling the last gaps to make truly free and private mobile phones a reality.